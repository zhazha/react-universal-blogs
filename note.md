***这个用于做项目的过程解决遇到的问题和疑惑的一个稿件***
exports和module.exports的区别：
    exports 是指向的 module.exports 的引用
    module.exports 初始值为一个空对象 {}，所以 exports 初始值也是 {}
    require() 返回的是 module.exports 而不是 exports
下面两种是等价的：
  exports = module.exports = somethings
  ||
  module.exports = somethings
  exports = module.exports